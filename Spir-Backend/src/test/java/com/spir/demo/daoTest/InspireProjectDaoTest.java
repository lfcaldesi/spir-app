package com.spir.demo.daoTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.spir.dao.InspireProjectDAO;
import com.spir.dao.InspireProjectDAOImpl;
import com.spir.entity.InspireProject;
import com.spir.repository.InspireProjectRepository;


@RunWith(MockitoJUnitRunner.class)

public class InspireProjectDaoTest {

	@Mock
	private InspireProjectRepository inspireProjectRepository;
	
	
	@InjectMocks
	private InspireProjectDAOImpl inspireProjectDAOImpl;
	
	
	// READ
		@Test
		public void getAllInspireProjects() {

			List<InspireProject> list = new ArrayList<InspireProject>();
			InspireProject i1 = new InspireProject(1, "spir1", 2, "type 1", "test", "blocked", "advanced", 2);
			InspireProject i2 = new InspireProject(2, "spir2", 4, "type 1", "test", "blocked", "advanced", 2);
			InspireProject i3 = new InspireProject(3, "spir3", 4, "type 3", "test", "pending", "advanced", 1);

			list.add(i1);
			list.add(i2);
			list.add(i3);

			when(inspireProjectRepository.findAll()).thenReturn(list);

			// test
			List<InspireProject> spirList = inspireProjectDAOImpl.findAllSpirProject();
			assertEquals(3, spirList.size());
			verify(inspireProjectRepository, times(1)).findAll();
		}
		
		// CREATE
		@Test
		public void createSpirProjectTest() {
			InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);

			inspireProjectDAOImpl.createSpirProject(i1);

			verify(inspireProjectRepository, times(1)).save(i1);
		}
		
		// DELETE
		@Test
		public void deleteSpirProjectTest() {
			InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);

			inspireProjectDAOImpl.deleteSpireProjectById(4);
			verify(inspireProjectRepository, times(1)).deleteById(i1.getSpringProjectId());
		}
		

		// NATIVE
		@Test
		public void findBySpirStatus() {

			InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);
			inspireProjectDAOImpl.findBySpirStatus("pending");
			verify(inspireProjectRepository, times(1)).findBySpirStatus(i1.getSpirStatus());

		}



}
