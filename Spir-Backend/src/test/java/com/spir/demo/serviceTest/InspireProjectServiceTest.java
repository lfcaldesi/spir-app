package com.spir.demo.serviceTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.spir.dao.InspireProjectDAO;
import com.spir.entity.InspireProject;
import com.spir.service.InspireServiceImpl;

//https://howtodoinjava.com/spring-boot2/testing/spring-boot-mockito-junit-example/

@RunWith(MockitoJUnitRunner.class)

public class InspireProjectServiceTest {

	@InjectMocks
	private InspireServiceImpl inspireServiceImpl;

	@Mock
	private InspireProjectDAO inspireProjectDAO;

	@Before(value = "")
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	// READ
	@Test
	public void getAllInspireProjects() {

		List<InspireProject> list = new ArrayList<InspireProject>();
		InspireProject i1 = new InspireProject(1, "spir1", 2, "type 1", "test", "blocked", "advanced", 2);
		InspireProject i2 = new InspireProject(2, "spir2", 4, "type 1", "test", "blocked", "advanced", 2);
		InspireProject i3 = new InspireProject(3, "spir3", 4, "type 3", "test", "pending", "advanced", 1);

		list.add(i1);
		list.add(i2);
		list.add(i3);

		when(inspireProjectDAO.findAllSpirProject()).thenReturn(list);

		// test
		List<InspireProject> spirList = inspireServiceImpl.findAllSpirProject();
		assertEquals(3, spirList.size());
		verify(inspireProjectDAO, times(1)).findAllSpirProject();
	}

	// CREATE
	@Test
	public void createSpirProjectTest() {
		InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);

		inspireServiceImpl.createSpirProject(i1);

		verify(inspireProjectDAO, times(1)).createSpirProject(i1);
	}

	// DELETE
	@Test
	public void deleteSpirProjectTest() {
		InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);

		inspireServiceImpl.deleteSpireProjectById(4);
		verify(inspireProjectDAO, times(1)).deleteSpireProjectById(4);
	}
	
	//UPDATE
	@Test
	public void updateSpirByIdTest() {
		InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);
		inspireServiceImpl.updateSpirProject(i1, i1.getSpringProjectId());//update the spir with id = 4
		verify(inspireProjectDAO,times(1)).updateSpirProject(i1, i1.getSpringProjectId());
	}
	

	// NATIVE
	@Test
	public void findBySpirStatus() {

		InspireProject i1 = new InspireProject(4, "spir4", 4, "type 3", "test", "pending", "advanced", 1);
		InspireProject i2 = new InspireProject(5, "spir5", 4, "type 3", "test", "blocked", "advanced", 1);
		inspireServiceImpl.findBySpirStatus("pending");
		verify(inspireProjectDAO, times(1)).findBySpirStatus("pending");

	}

}
