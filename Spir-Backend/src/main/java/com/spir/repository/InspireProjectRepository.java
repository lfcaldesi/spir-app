package com.spir.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.spir.entity.InspireProject;

public interface InspireProjectRepository extends JpaRepository<InspireProject, Integer> {

	
	//with native query
	@Query(value = "SELECT * FROM inspire_project WHERE spir_status = ?1", nativeQuery = true)
	List < InspireProject >findBySpirStatus(String spirStatus);
	
	@Query(value = "SELECT * FROM inspire_project WHERE spir_project_id= ?1", nativeQuery = true)
	 InspireProject  findSpirById(int id);
	
	

	
}
