package com.spir.controller;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spir.entity.InspireProject;
import com.spir.entity.User;
import com.spir.service.InspireService;

@CrossOrigin("*")
@RestController
public class SpirController {

	private static final String EXCEL_FILE_NAME = "Excel Spir file report";
	@Autowired
	private InspireService inspireService;

	// @Secured("ROLE_USER") //assigned in application.properties
	@GetMapping(produces = "application/json")
	@RequestMapping({ "spir/validateLogin" })
	public User validateLogin() {
		return new User("User successfully authenticated");
	}

	// CREATE
	@PostMapping("/spir")
	public ResponseEntity<Object> createSpir(@RequestBody InspireProject inspirePorject) {
		return inspireService.createSpirProject(inspirePorject);

	}

	// READ
	@GetMapping("/spir")
	public List<InspireProject> retrieveAllSpire() {
		return inspireService.findAllSpirProject();
	}

	@GetMapping("/spir/{id}")
	public InspireProject retrieveSpirById(@PathVariable(value = "id") int id) {
		return inspireService.findSpirById(id);

	}

//	}

	// CLONE
	@PostMapping("/spir/{id}")
	public InspireProject clone(@PathVariable(value = "id") int id) {
		return inspireService.clone(id);

	}

	// EXCEL

	@RequestMapping(value = "/downloadExcel", method = RequestMethod.GET)
	public ModelAndView downloadExcel() {
		// create some sample data
		List<InspireProject> listSpirs = new ArrayList<InspireProject>();
		listSpirs.add(new InspireProject(696, "Spir2131", 0, "CSA", "Blocked", null, null, 0));
		listSpirs.add(new InspireProject(996, "Spir213s1", 0, "CSA", "active", null, null, 0));

		// return a view which will be resolved by an excel view resolver
		return new ModelAndView("excelView", "listSpirs", listSpirs);
	}

	// UPDATE
	@PutMapping("/spir/{id}")
	public ResponseEntity<Object> updateSPIR(@RequestBody InspireProject inspireProject,
			@PathVariable(value = "id") int id) {// put in the body what to modify through form for example
		return inspireService.updateSpirProject(inspireProject, id);

	}

	// DELETE

	@DeleteMapping("/spir/{id}")
	public void deleteSPIR(@PathVariable(value = "id") Integer id) {
		inspireService.deleteSpireProjectById(id);
	}

	// native query example
	@GetMapping("/spir/native/{spirStatus}")
	public List<InspireProject> findBySpirStatus(@PathVariable String spirStatus) {
		return inspireService.findBySpirStatus(spirStatus);
	}

	@PostMapping("spir/uploadFile")
	@ResponseBody
	public boolean upload(@RequestParam("file") MultipartFile file) {
		try {
			if (file.isEmpty() == false) {
				System.out.println("Successfully Uploaded: " + file.getOriginalFilename());

				return true;
			} else {
				System.out.println("ERROR");
				return false;
			}
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	
	//DOWNLOAD  EXCEL
	@GetMapping("spir/download")
	public void downloadSingle(HttpServletResponse response) throws Exception {
		List<InspireProject> spirList = inspireService.findAllSpirProject();

		Workbook workbook = new HSSFWorkbook();
		
		// create a new Excel sheet
		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("All Applications List");
		sheet.setDefaultColumnWidth(30);
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		
		
		HSSFRow header = sheet.createRow(0);
		header.createCell(0).setCellValue("ID");
		header.createCell(1).setCellValue("NAME");
		header.createCell(2).setCellValue("SPIR TYPE");
		header.createCell(3).setCellValue("SPIR STATUS");
		int rowNum = 1;
		for (InspireProject spir : spirList) {
			Row aRow = sheet.createRow(rowNum++);
			aRow.createCell(0).setCellValue(spir.getSpringProjectId());
			aRow.createCell(1).setCellValue(spir.getName());
			aRow.createCell(2).setCellValue(spir.getSpirType());
			aRow.createCell(3).setCellValue(spir.getSpirStatus());
		}

		response.setHeader("Content-disposition", "attachment; filename=" + EXCEL_FILE_NAME+".xls");
		workbook.write(response.getOutputStream());
	}

	
	
	//DOWNLOAD SINGLE  EXCEL
		@GetMapping("spir/download/{id}")
		public void download(HttpServletResponse response,@PathVariable(value = "id") int id) throws Exception {
		InspireProject spir = inspireService.findSpirById(id);

			Workbook workbook = new HSSFWorkbook();
			
			// create a new Excel sheet
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("All Applications List");
			sheet.setDefaultColumnWidth(30);
			// create style for header cells
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setFontName("Arial");
			style.setFillForegroundColor(HSSFColor.BLUE.index);
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);
			style.setFont(font);
			
			
			HSSFRow header = sheet.createRow(0);
			header.createCell(0).setCellValue("ID");
			header.createCell(1).setCellValue("NAME");
			header.createCell(2).setCellValue("SPIR TYPE");
			header.createCell(3).setCellValue("SPIR STATUS");
			header.createCell(4).setCellValue("ORGANIZATION ID");
			header.createCell(5).setCellValue("SPIR PHASE");
			int rowNum = 1;
			
				Row aRow = sheet.createRow(rowNum++);
				aRow.createCell(0).setCellValue(spir.getSpringProjectId());
				aRow.createCell(1).setCellValue(spir.getName());
				aRow.createCell(2).setCellValue(spir.getSpirType());
				aRow.createCell(3).setCellValue(spir.getSpirStatus());
				aRow.createCell(4).setCellValue(spir.getSpirOrganizationId());
				aRow.createCell(5).setCellValue(spir.getSpirPhase());
			

			response.setHeader("Content-disposition", "attachment; filename=" +"SPIR_"+spir.getName()+"_report"+".xls");
			workbook.write(response.getOutputStream());
		}

	
}
