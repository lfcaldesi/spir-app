package com.spir.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class InspireProject {

	@Id
	@GeneratedValue
	private int spirProjectId;

	private String name;

	private int stepIndicator;

	private String spirType;

	private String spirSparePartType;

	private String spirStatus;

	private String spirPhase;
	
	private int spirOrganizationId;

	public int getSpringProjectId() {
		return spirProjectId;
	}

	public void setSpringProjectId(int springProjectId) {
		this.spirProjectId = springProjectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStepIndicator() {
		return stepIndicator;
	}

	public void setStepIndicator(int stepIndicator) {
		this.stepIndicator = stepIndicator;
	}

	public String getSpirType() {
		return spirType;
	}

	public void setSpirType(String spirType) {
		this.spirType = spirType;
	}

	public String getSpirSparePartType() {
		return spirSparePartType;
	}

	public void setSpirSparePartType(String spirSparePartType) {
		this.spirSparePartType = spirSparePartType;
	}

	public String getSpirStatus() {
		return spirStatus;
	}

	public void setSpirStatus(String spirStatus) {
		this.spirStatus = spirStatus;
	}

	public String getSpirPhase() {
		return spirPhase;
	}

	public void setSpirPhase(String spirPhase) {
		this.spirPhase = spirPhase;
	}

	public int getSpirOrganizationId() {
		return spirOrganizationId;
	}

	public void setSpirOrganizationId(int spirOrganizationId) {
		this.spirOrganizationId = spirOrganizationId;
	}

	public InspireProject(int spirProjectId, String name, int stepIndicator, String spirType, String spirSparePartType,
			String spirStatus, String spirPhase, int spirOrganizationId) {
		super();
		this.spirProjectId = spirProjectId;
		this.name = name;
		this.stepIndicator = stepIndicator;
		this.spirType = spirType;
		this.spirSparePartType = spirSparePartType;
		this.spirStatus = spirStatus;
		this.spirPhase = spirPhase;
		this.spirOrganizationId = spirOrganizationId;
	}
	
	
	public InspireProject() {}
	
	
	

}
