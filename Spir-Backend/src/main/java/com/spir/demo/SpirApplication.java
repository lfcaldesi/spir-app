package com.spir.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@ComponentScan({"com.spir.controller","com.spir.demo","com.spir.service","com.spir.dao","com.spir.jwt","com.spir.jwt.resource"})
@EnableJpaRepositories("com.spir.repository")
@EnableAutoConfiguration
@EntityScan("com.spir.entity")
public class SpirApplication {
//
//	@Bean
//	BCryptPasswordEncoder bCryptPasswordEncoder() {
//		return new BCryptPasswordEncoder();
//	}
//	
	public static void main(String[] args) {
		SpringApplication.run(SpirApplication.class, args);
	}

}
