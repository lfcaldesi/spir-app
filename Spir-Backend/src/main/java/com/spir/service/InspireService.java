package com.spir.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.spir.entity.InspireProject;

public interface InspireService {
	public List<InspireProject> findAllSpirProject();
	
	public ResponseEntity<Object> createSpirProject(InspireProject inspireProject);
	
	public void deleteSpireProjectById(int spireProjectId);
	
	public ResponseEntity<Object> updateSpirProject(InspireProject inspireProject, int  spireProjectId);

	public List<InspireProject> findBySpirStatus(String spirStatus);

	

	public InspireProject findSpirById(int id);
	

	public InspireProject clone(int id);

}
