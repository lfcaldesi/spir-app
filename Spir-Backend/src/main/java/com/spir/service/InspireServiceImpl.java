package com.spir.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spir.dao.InspireProjectDAO;
import com.spir.entity.InspireProject;

@Service
@Transactional

public class InspireServiceImpl implements InspireService {
	
@Autowired
private InspireProjectDAO inspireProjectDAO;


public List<InspireProject> findAllSpirProject() {
	return inspireProjectDAO.findAllSpirProject();}

@Override
public ResponseEntity<Object> createSpirProject(InspireProject inspireProject) {
	return inspireProjectDAO.createSpirProject(inspireProject);
}

@Override
public void deleteSpireProjectById(int spireProjectId) {
	inspireProjectDAO.deleteSpireProjectById(spireProjectId);
	
}

@Override
public ResponseEntity<Object> updateSpirProject(InspireProject inspireProject, int spireProjectId) {
	return inspireProjectDAO.updateSpirProject(inspireProject, spireProjectId);
	}


@Override
public List < InspireProject >findBySpirStatus(String spirStatus){
	return inspireProjectDAO.findBySpirStatus(spirStatus);
}


@Override

public InspireProject findSpirById(int id) {
	return inspireProjectDAO.findSpirById(id);
}

@Override

public InspireProject clone(int id) {
	return inspireProjectDAO.clone(id);
}



}
