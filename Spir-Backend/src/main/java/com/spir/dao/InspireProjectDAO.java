package com.spir.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.spir.entity.InspireProject;
import com.spir.repository.InspireProjectRepository;

public interface InspireProjectDAO {

	public List<InspireProject> findAllSpirProject();

	public ResponseEntity<Object> createSpirProject(InspireProject inspireProject);

	public void deleteSpireProjectById(int spireProjectId);

	public ResponseEntity<Object> updateSpirProject(InspireProject inspireProject, int spireProjectId);

	public List<InspireProject> findBySpirStatus(String spirStatus);
	
	public InspireProject findSpirById(int id);
	 public InspireProject clone (int id);
}
