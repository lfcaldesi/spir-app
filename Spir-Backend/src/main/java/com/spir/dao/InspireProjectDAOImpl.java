package com.spir.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.spir.entity.InspireProject;
import com.spir.repository.InspireProjectRepository;

@Component
public class InspireProjectDAOImpl implements InspireProjectDAO {
	@Autowired
	InspireProjectRepository inspireProjectRepository;

//READ
	@Override
	public List<InspireProject> findAllSpirProject() {
		return inspireProjectRepository.findAll();

	}

//CREATE
	@Override
	public ResponseEntity<Object> createSpirProject(InspireProject inspireProject) {
		InspireProject savedInspireProject = inspireProjectRepository.save(inspireProject);

		return ResponseEntity.ok(savedInspireProject);

	}
	

//DELETE
	@Override
	public void deleteSpireProjectById(int spireProjectId) {
		inspireProjectRepository.deleteById(spireProjectId);

	}

//UPDATE
	@Override

	public ResponseEntity<Object> updateSpirProject(InspireProject inspireProject, int spireProjectId) {

		java.util.Optional<InspireProject> inspireOptional = inspireProjectRepository.findById(spireProjectId);

		if (!inspireOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}else {
		inspireProject.setSpringProjectId(spireProjectId);

		inspireProjectRepository.save(inspireProject);

		return ResponseEntity.noContent().build();
	}
	}
//native query
	@Override
	public List<InspireProject> findBySpirStatus(String spirStatus) {
		return inspireProjectRepository.findBySpirStatus(spirStatus);

	}
	

	//FIND BY ID with native
	@Override
	public InspireProject findSpirById(int id) {
		return inspireProjectRepository.findSpirById(id);
	}
	
	
	//CLONE
	@Override
	public InspireProject clone(int id)			 {
		InspireProject orig = inspireProjectRepository.findById(id).get();
		InspireProject clone = new InspireProject();
		clone.setName(orig.getName()+"_cloned");
		clone.setSpirType(orig.getSpirType());
		clone.setSpirStatus(orig.getSpirStatus());
		inspireProjectRepository.save(clone);
		return clone
	;
	


}
	


}
