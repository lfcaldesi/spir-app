import { Component, OnInit } from "@angular/core";
import { Spir } from "../spir";
import { ActivatedRoute, Router } from "@angular/router";

import { SpirService } from "../spir.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { SpirStatus } from "../enums";

@Component({
  selector: "app-update-spir",
  templateUrl: "./update-spir.component.html",
  styleUrls: ["./update-spir.component.css"]
})
export class UpdateSpirComponent implements OnInit {
  id: number;
  spir: Spir = new Spir();
  registerForm: FormGroup;
  spirs: Spir[];
  spirsStatusEnum = SpirStatus;
  spir_status = Object.values(this.spirsStatusEnum); //enums for status
  //or//  spir_status = Object.values(this.spirsStatusEnum).filter(e => typeof e == "string" );

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private spirService: SpirService
  ) {
    this.spir = new Spir();
  }

  ngOnInit() {
    this.id = this.route.snapshot.params["id"]; //get id from url

    this.registerForm = this.formBuilder.group({
      name: ["", Validators.required],
      spirType: ["", Validators.required],
      spirStatus: ["", Validators.required]
    });

    //get spir(when update) previous details using
    // this.spir = this.spirService.getter(); added findByid method so it is not needed
    this.spirService.getSpir(this.id).subscribe(
      data => {
        console.log(data);
        this.spir = data;
        if (data == null) {
          //debugger; //launch the excpetion and see ctrl*shf+i that it will stops
          alert(" Spir with id " + this.id + " does not exist");

          this.gotoList();
        }
      },
      (error: Response) => {
        if (error.status === 400) {
          alert("Error 400");
          this.gotoList();
        } else {
          alert("An Unexpected Error Occured.");
          console.log(error);
          this.gotoList();
        }
      }
    );
  }
  updateSpir() {
    this.spirService.updateSpir(this.id, this.spir).subscribe(data => {
      console.log(data);
      this.gotoList();
    });
  }

  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }
    alert("SPIR '" + this.spir.name + "' HAS BEEN MODIFIED SUCCESSFULLY");
    this.updateSpir();
  }

  gotoList() {
    this.router.navigate(["/spirs"]);
  }
}
