import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSpirComponent } from './update-spir.component';

describe('UpdateSpirComponent', () => {
  let component: UpdateSpirComponent;
  let fixture: ComponentFixture<UpdateSpirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSpirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSpirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
