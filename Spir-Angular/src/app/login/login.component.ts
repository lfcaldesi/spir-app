import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { AuthenticationService } from "../authentication-service.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  invalidLogin = false;

  constructor(
    private router: Router,
    private loginservice: AuthenticationService
  ) {}

  ngOnInit() {}

  checkLogin() {
    this.loginservice.JWTauthenticate(this.username, this.password).subscribe(
      data => {
        console.log(data);
        this.router.navigate(["spirs"]);
        this.invalidLogin = false;
      },
      error => {
        this.loginservice.errorMsg(
          error.message + " Login Failed. Please retry."
        );
        //alert("Login failed !");
        this.invalidLogin = true;
      }
    );
  }
}
