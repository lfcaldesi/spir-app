import { Pipe, PipeTransform } from "@angular/core";
import { Spir } from "../spir";
import { SpirStatus, SpirType } from "../enums";
/*
@Pipe({
  name: "filter"
})
export class FilterPipe implements PipeTransform {
  transform(value: any, arg: any[]): any { //arg is the search field value is the value to retrieve(the obkect)
    const resultSpir = [];
    for (const spir of value) {  //call the value as the spir
      if (  
        spir.name.toLowerCase().indexOf(arg) > -1  //if the arg>-1(-1 il the 1st character that hace to match)
        
      ) {
        resultSpir.push(spir);//if is >-1(so there is a match) push in the resultARRAY this value
      }
    }
    return resultSpir;
  }
}*/
@Pipe({
  name: "filter",
  pure: false
})
export class FilterPipe implements PipeTransform {
  transform(
    spirs: Spir[],
    nameSearch: string,
    statusSearch: SpirStatus,
    typeSearch: SpirType
  ) {
    if (spirs && spirs.length) {
      return spirs.filter(value => {
        if (
          nameSearch &&
          value.name.toLowerCase().indexOf(nameSearch.toLowerCase()) === -1
        ) {
          return false; //if the starting point (-1 (index of array)) doesn't have result return false
        }
        if (statusSearch && value.spirStatus.indexOf(statusSearch) === -1) {
          return false;
        }
        if (
          typeSearch &&
          value.spirType.toLowerCase().indexOf(typeSearch.toLowerCase()) === -1
        ) {
          return false;
        }

        return true;
      });
    } else {
      return spirs; //else return the array of spir that match the condition
    }
  }
}
