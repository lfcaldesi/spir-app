import { NgModule } from "@angular/core";
import { Routes, RouterModule, Router } from "@angular/router";
import { CreateSpirComponent } from "./create-spir/create-spir.component";
import { SpirListComponent } from "./spir-list/spir-list.component";
import { UpdateSpirComponent } from "./update-spir/update-spir.component";
import { FindByStatusComponent } from "./find-by-status/find-by-status.component";
import { PageNotFooundComponent } from "./page-not-foound/page-not-foound.component";
import { LoginComponent } from "./login/login.component";
import { AuthGaurdService } from "./authGaurd.service";
import { LogoutComponent } from "./logout/logout.component";

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "spirs",
    component: SpirListComponent,
    canActivate: [AuthGaurdService]
  },

  {
    path: "add",
    component: CreateSpirComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: "update/:id",
    component: UpdateSpirComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: "find",
    component: FindByStatusComponent,
    canActivate: [AuthGaurdService]
  },
  { path: "login", component: LoginComponent },
  { path: "logout", component: LogoutComponent },
  { path: "**", redirectTo: "/404" },
  { path: "404", component: PageNotFooundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
