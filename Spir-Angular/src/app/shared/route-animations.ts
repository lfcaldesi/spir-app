import { transition, trigger, animate, style } from "@angular/animations";

export const routeFadeStateTrigger = trigger("routeFadeState", [
  transition(":enter", [
    style({
      opacity: 0
    }),
    animate(3000)
  ]),
  transition(
    ":leave",
    animate(
      3000,
      style({
        opacity: 0
      })
    )
  )
]);
