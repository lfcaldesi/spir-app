import { NgModule } from "@angular/core";
import { LoginComponent } from "./login/login.component";
import { LogoutComponent } from "./logout/logout.component";
import { MaterialModule } from "./material.module";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { BasicAuthHtppInterceptorService } from "./basic-auth-http-interceptor.service";
@NgModule({
  declarations: [LoginComponent, LogoutComponent],
  imports: [MaterialModule, CommonModule, FormsModule, ReactiveFormsModule],
  exports: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthHtppInterceptorService,
      multi: true
    }
  ]
})
export class AuthModule {}
