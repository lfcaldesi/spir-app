import { Component, OnInit } from "@angular/core";
import { Route } from "@angular/compiler/src/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-page-not-foound",
  templateUrl: "./page-not-foound.component.html",
  styleUrls: ["./page-not-foound.component.css"]
})
export class PageNotFooundComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  goToList() {
    this.router.navigate(["/spirs"]);
  }
}
