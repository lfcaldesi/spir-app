import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { PageNotFooundComponent } from "./page-not-foound/page-not-foound.component";

import { HeaderComponent } from "./header/header.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { StoreModule } from "@ngrx/store";
import { appReducer } from "./app.reducer";
import { AuthModule } from "./auth.module";
import { SpirModule } from "./spir.module";
@NgModule({
  declarations: [AppComponent, PageNotFooundComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AuthModule,
    SpirModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    StoreModule.forRoot({ ui: appReducer })
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
