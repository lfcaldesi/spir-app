import { Component, OnInit, HostBinding } from "@angular/core";
import { Spir } from "../spir";
import { Observable } from "rxjs";
import { SpirService } from "../spir.service";
import { Router } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { SpirStatus, SpirType } from "../enums";
import { HttpClient, HttpHeaders, HttpEventType } from "@angular/common/http";
import { routeFadeStateTrigger } from "../shared/route-animations";
@Component({
  selector: "app-create-spir",
  templateUrl: "./create-spir.component.html",
  styleUrls: ["./create-spir.component.css"],
  animations: [routeFadeStateTrigger]
})
export class CreateSpirComponent implements OnInit {
  @HostBinding("@routeFadeState") routeAnimation = true;

  registerForm: FormGroup;
  submitted = false;
  spir: Spir = new Spir();
  spirsStatusEnum = SpirStatus;
  //spir_status = ["active", "pending", "blocked"];
  spir_status = Object.values(this.spirsStatusEnum); //enums for status
  forbiddenSpirNames = ["Spir", "Tryal"];
  spirTypesEnum = SpirType;
  spir_type = Object.values(this.spirTypesEnum);
  spirs: Observable<Spir[]>;

  selectedFile: File = null;
  constructor(
    private formBuilder: FormBuilder,
    private spirService: SpirService,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.spir.spirType = SpirType.CSA;
    this.registerForm = this.formBuilder.group({
      name: ["", [Validators.required, this.forbiddenNames.bind(this)]],
      spirType: ["", Validators.required],
      spirStatus: ["", Validators.required]
    });
  }

  //custom validator
  forbiddenNames(control: FormControl): { [s: string]: boolean } {
    if (this.forbiddenSpirNames.indexOf(control.value) !== -1) {
      return { nameIsForbidden: true };
    }
    return null;
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
  }
  onUpload() {
    const fd = new FormData();
    fd.append("file", this.selectedFile, this.selectedFile.name); //the name file is the request param from backend
    return this.http
      .post("http://localhost:8080/spir/uploadFile", fd, {
        reportProgress: true,
        observe: "events"
      })

      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          console.log(
            "Upload Progress:" +
              Math.round((event.loaded / event.total) * 100) +
              "%"
          );
        else if (event.type === HttpEventType.Response) {
          console.log(event);
        }
      });
  }

  newSpir(): void {
    this.submitted = false;
    this.spir = new Spir();
  }
  reloadData() {
    this.spirs = this.spirService.getSpirsList();
  }

  save() {
    this.spirService.createSpir(this.spir).subscribe(
      data => console.log(data),
      error => console.log(error)
    );
    this.spir = new Spir();
    this.reloadData();
    this.gotoList(); //after pushing create butotn goes to main page
    //
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      alert("invalid form.Please fill all the fields required");

      return;
    }
    alert("SPIR '" + this.spir.name + "' HAS BEEN CREATED SUCCESSFULLY");
    this.save();
    this.reloadData();
  }
  gotoList() {
    this.router.navigate(["/spirs"]);
  }
}
