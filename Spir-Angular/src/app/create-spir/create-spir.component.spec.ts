import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CreateSpirComponent } from "./create-spir.component";

describe("CreateSpirComponent", () => {
  let component: CreateSpirComponent;
  let fixture: ComponentFixture<CreateSpirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateSpirComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
