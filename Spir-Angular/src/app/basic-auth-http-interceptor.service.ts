import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler
} from "@angular/common/http";
import { AuthenticationService } from "./authentication-service.service";

@Injectable({
  providedIn: "root"
})
export class BasicAuthHtppInterceptorService implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    /* if (
      sessionStorage.getItem("username") &&
      sessionStorage.getItem("basicauth")
    ) {
      req = req.clone({
        setHeaders: {
          Authorization: sessionStorage.getItem("basicauth")
        }
      });*/

    let tokenAuthHeaderString = this.authenticationService.getAuthenticationToken();
    let username = this.authenticationService.getAuthenticatedUser();

    if (tokenAuthHeaderString && username) {
      req = req.clone({
        setHeaders: {
          Authorization: tokenAuthHeaderString
        }
      });
    }

    return next.handle(req);
  }
}
