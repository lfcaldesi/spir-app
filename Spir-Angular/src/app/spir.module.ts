import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SpirListComponent } from "./spir-list/spir-list.component";
import { CreateSpirComponent } from "./create-spir/create-spir.component";
import { ShortenPipe } from "./pipes/shorten.pipe";
import { SortPipe } from "./pipes/sort.pipe";
import { loadingSpinnerComponent } from "./loading-spinner/loading-spinner.component";
import { FindByStatusComponent } from "./find-by-status/find-by-status.component";
import { FilterPipe } from "./pipes/filter.pipe";
import { UpdateSpirComponent } from "./update-spir/update-spir.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { InactivityTimerComponent } from "./inactivity-timer.component";
@NgModule({
  declarations: [
    SpirListComponent,
    CreateSpirComponent,
    SortPipe,
    ShortenPipe,
    loadingSpinnerComponent,
    UpdateSpirComponent,
    FindByStatusComponent,
    FilterPipe,
    InactivityTimerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    HttpClientModule,
    AppRoutingModule
  ],
  exports: []
})
export class SpirModule {}
