import { TestBed } from '@angular/core/testing';

import { SpirService } from './spir.service';

describe('SpirService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpirService = TestBed.get(SpirService);
    expect(service).toBeTruthy();
  });
});
