export enum SpirStatus {
  PENDING = "pending",
  BLOCKED = "blocked",
  ACTIVE = "active"
}
