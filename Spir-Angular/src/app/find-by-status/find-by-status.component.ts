import { Component, OnInit } from "@angular/core";
import { Spir } from "../spir";
import { SpirService } from "../spir.service";
import { Router } from "@angular/router";
import { SpirStatus } from "../enums";

@Component({
  selector: "app-find-by-status",
  templateUrl: "./find-by-status.component.html",
  styleUrls: ["./find-by-status.component.css"]
})
export class FindByStatusComponent implements OnInit {
  spirStatus: string;
  spirs: Spir[];
  spirsStatusEnum = SpirStatus;
  spir_status = Object.values(this.spirsStatusEnum);

  //spir_status1 = ["pending", "blocked", "active"];
  constructor(private spirService: SpirService, private router: Router) {}

  ngOnInit() {
    this.spirStatus = "";
    //this.reloadData();
  }

  private searchSpirs() {
    this.spirService
      .getSpirByStatus(this.spirStatus)
      .subscribe(spirs => (this.spirs = spirs));
    if (this.spirs == null) {
      alert("no Spir found for that status " + this.spirStatus);
    }
  }

  onSubmit() {
    this.searchSpirs();
  }

  reloadData() {
    this.spirService.getSpirsList().subscribe(data => (this.spirs = data));
  }

  deleteSpir(spir: Spir) {
    var retVal = confirm(
      "Are you sure you want to delete the Spir with name '" + spir.name + "' ?"
    );
    if (retVal == true) {
      // Display a confirmation prompt

      this.spirService.deleteSpir(spir.springProjectId).subscribe(
        data => {
          console.log(data);
          this.onSubmit();
          // this.gotoList();
        },
        error => console.log(error)
      );
      alert("Spir '" + spir.name + "' has been deleted successfully!");
    } else {
      alert("Operation canceled!");
    }
  }

  updateSpir(spir: Spir) {
    this.spirService.setter(spir); //added to fetch data

    this.router.navigate(["update", spir.springProjectId]);
  }
  gotoList() {
    this.router.navigate(["/spirs"]);
  }
}
