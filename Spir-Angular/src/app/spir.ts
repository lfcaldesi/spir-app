import { SpirStatus, SpirType } from "./enums";

export class Spir {
  name: string;
  spirOrganizationId: number;
  spirPhase: string;
  spirSparePartType: string;
  spirStatus: SpirStatus;
  spirType: SpirType;
  springProjectId: number;
  stepIndicator: number;
}
