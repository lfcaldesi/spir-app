import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SpirListComponent } from "./spir-list.component";

describe("SpirListComponent", () => {
  let component: SpirListComponent;
  let fixture: ComponentFixture<SpirListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpirListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpirListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
