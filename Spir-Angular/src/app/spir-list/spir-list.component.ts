import { Component, OnInit, HostListener } from "@angular/core";
import { Observable } from "rxjs";
import { Spir } from "../spir";
import { Router } from "@angular/router";
import { SpirService } from "../spir.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SpirStatus, SpirType } from "../enums";
import {
  state,
  transition,
  trigger,
  animate,
  style
} from "@angular/animations";
import { AuthenticationService } from "../authentication-service.service";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: "app-spir-list",
  templateUrl: "./spir-list.component.html",
  styleUrls: ["./spir-list.component.css"],
  animations: [
    trigger("fade", [
      transition("void=>*", [
        style({ backgroundColor: "lightblue", opacity: 0 }),
        animate(2000, style({ backgroundColor: "white", opacity: 1 }))
      ])
    ])
  ]
})
export class SpirListComponent implements OnInit {
  spirs: Spir[];
  spirsStatusEnum = SpirStatus;
  spirsTypesEnum = SpirType;

  //bh filter
  spir_status1 = Object.values(this.spirsStatusEnum);

  constructor(
    private formBuilder: FormBuilder,
    private spirService: SpirService,
    private router: Router,
    private authService: AuthenticationService,
    private _http: HttpClient
  ) {
    this.router.errorHandler = (error: any) => {
      this.router.navigate(["404"]);
    };
  }
  showModal: boolean;
  isLoading: boolean;
  nameSearch = "";

  statusEnums = Object.values(this.spirsStatusEnum);
  statusSearch = "";

  typesEnums = Object.values(this.spirsTypesEnum);
  typeSearch = "";
  id: number;
  spirStatus1: string;
  spir: Spir = new Spir();
  cloned_spir: Spir = new Spir();

  // Object to save the response returned from the service.
  myresponse: any;

  ngOnInit() {
    this.isLoading = false;
    this.spirStatus1 = ""; //BACKEND FILTER

    this.reloadData();
  }

  //timer for logging out after 1 minute
  @HostListener("document:keyup", ["$event"])
  @HostListener("document:click", ["$event"])
  @HostListener("document:wheel", ["$event"])
  resetTimer() {
    this.authService.notifyUserAction();
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.spirs, event.previousIndex, event.currentIndex);
  }

  resetFilter() {
    this.nameSearch = "";
    this.statusSearch = "";
    this.typeSearch = "";
  }

  getSpirById(spir: Spir) {
    this.spirService.getSpir(spir.springProjectId).subscribe(data => {
      this.spir = data;
      console.log(spir.springProjectId);
    });
  }

  reloadData() {
    this.isLoading = true;
    this.spirService.getSpirsList().subscribe(
      data => {
        this.spirs = data;
        this.isLoading = false;
      },
      error => {
        this.isLoading = false;
        alert("an error occurred");
        console.log(error);
      }
    );
  }
  //DELETE
  deleteSpir(spir: Spir) {
    var retVal = confirm(
      "Are you sure you want to delete the Spir with name '" + spir.name + "' ?"
    );
    if (retVal == true) {
      // Display a confirmation prompt

      this.spirService.deleteSpir(spir.springProjectId).subscribe(
        data => {
          console.log(data);
          this.reloadData();
          // this.gotoList();
        },
        error => console.log(error)
      );
      alert("Spir '" + spir.name + "' has been deleted successfully!");
    } else {
      alert("Operation canceled!");
    }
  }

  //BACKEND FILTER
  onSubmit1() {
    this.searchSpirs();
  }
  private searchSpirs() {
    this.spirService.getSpirByStatus(this.spirStatus1).subscribe(spir => {
      this.spirs = spir;

      if (this.spirs.length == 0) {
        this.spirStatus1 = "";
        alert("No spir with status: " + this.spirStatus1 + " found");
        this.reloadData();
      }
    });
  }

  showDetails(spir: Spir) {
    alert("spare type is: " + spir.spirSparePartType);
  }
  //UPDATE
  updateSpir(spir: Spir) {
    this.spirService.updateSpir(spir.springProjectId, spir);
    //this.spirService.setter(spir);

    this.router.navigate(["update", spir.springProjectId]);
  }

  //CREATE
  createSpir() {
    this.router.navigate(["add"]);
  }
  gotoList() {
    this.router.navigate(["/spirs"]);
  }

  clone(spir: Spir) {
    var retVal = confirm(
      "Are you sure you want to clone the Spir with name '" + spir.name + "' ?"
    );
    if (retVal == true) {
      this.spirService.cloneSpir(spir.springProjectId, this.spir).subscribe(
        data => console.log(data),
        error => console.log(error)
      );

      this.reloadData();
      this.gotoList();
      //
    } else {
      alert("operation canceled");
    }
  }

  getExcel() {
    this._http
      .get("http://localhost:8080/spir/download", { responseType: "text" })
      .subscribe(
        data => {
          this.myresponse = data;
          window.open("http://localhost:8080/spir/download", "_blank");
        },
        error => {
          console.log("Error occured", error);
        }
      );
  }

  getSingleExcel(spir: Spir) {
    this._http
      .get("http://localhost:8080/spir/download/" + spir.springProjectId, {
        responseType: "text"
      })
      .subscribe(
        data => {
          this.myresponse = data;
          window.open(
            "http://localhost:8080/spir/download/" + spir.springProjectId,
            "_blank"
          );
        },
        error => {
          console.log("Error occured", error);
        }
      );
  }
}
