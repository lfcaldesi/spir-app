import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Observable } from "rxjs";
import { Spir } from "./spir";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class SpirService {
  private baseUrl = "http://localhost:8080/spir";
  private spir: Spir;
  constructor(private http: HttpClient) {}

  isUserLoggedIn() {
    let user = sessionStorage.getItem("username");
    console.log("ok");
    return !(user === null);
  }

  //find by id
  getSpir(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createSpir(spir: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, spir);
  }

  updateSpir(id: number, spir: Spir): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, spir);
  }

  cloneSpir(id: number, spir: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/${id}`, spir);
  }

  deleteSpir(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
  getSpirsList(): Observable<Spir[]> {
    return this.http.get<Spir[]>(`${this.baseUrl}`);
  }
  getSpirByStatus(spirStatus: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/native/${spirStatus}`);
  }

  setter(spir: Spir) {
    this.spir = spir;
  }
  getter() {
    return this.spir;
  }
}
