import { Injectable } from "@angular/core";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";

export class User {
  constructor(public status: string) {}
}

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private snackbar: MatSnackBar
  ) {}
  /* Basic Auth
  public authenticate(username: string, password: string) {
    const headers = new HttpHeaders({
      Authorization: "Basic " + btoa(username + ":" + password)
    });
    return this.httpClient
      .get<User>("http://localhost:8080/spir/validateLogin", {
        headers,
        responseType: "text" as "json"
      })
      .pipe(
        map(userData => {
          console.log(userData);
          sessionStorage.setItem("username", username);
          let authString = "Basic " + btoa(username + ":" + password);
          sessionStorage.setItem("basicauth", authString);
          return userData;
        })
      );
  }
*/

  public JWTauthenticate(username, password) {
    return this.httpClient
      .post<any>("http://localhost:8080/authenticate", { username, password })
      .pipe(
        map(userData => {
          sessionStorage.setItem("username", username);

          sessionStorage.setItem("token", "Bearer " + userData.token);
          return userData;
        })
      );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem("username");
    console.log(!(user === null));
    return !(user === null);
  }
  errorMsg(errosMsg: string) {
    this.snackbar.open(errosMsg, null, { duration: 3000 });
  }
  getAuthenticatedUser() {
    return sessionStorage.getItem("username");
  }
  getAuthenticationToken() {
    if (this.getAuthenticatedUser()) return sessionStorage.getItem("token");
  }

  logOut() {
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("token");
    this.snackbar.open("User logged out", null, { duration: 3000 });
  }

  _userActionOccured: Subject<void> = new Subject();
  get userActionOccured(): Observable<void> {
    return this._userActionOccured.asObservable();
  }

  notifyUserAction() {
    this._userActionOccured.next();
  }

  loginUser() {
    console.log("user login");
  }

  logOutUser() {
    console.log("user logout");
    //set dark mode
    //document.getElementById("body").classList.toggle("dark-mode");
    sessionStorage.removeItem("username");
    alert("Session expired!");
    this.router.navigate(["login"]);
  }
}
