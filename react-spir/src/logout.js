import React, { Component } from "react";
import AuthenticationService from "./AuthenticationService";
import { Button } from "reactstrap";
import { withRouter } from "react-router-dom";
class Logout extends Component {
  constructor(props) {
    super(props);

    this.logoutClicked = this.logoutClicked.bind(this);
  }

  logoutClicked() {
    AuthenticationService.logout();
    console.log("logout successfully");

    this.props.history.push(`/login`);
  }

  render() {
    return (
      <div>
        <Button
          className="my-3"
          color="danger"
          onClick={this.logoutClicked.bind(this)}
        >
          Logout
        </Button>
      </div>
    );
  }
}

export default withRouter(Logout);
