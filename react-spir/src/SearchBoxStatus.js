import React from "react";
import { Input } from "reactstrap";
function SearchBoxStatus(props) {
  return (
    <Input type="select" onChange={props.handleChangeStatus}>
      <option value="">--Search By status--</option>
      <option value="pending">pending</option>
      <option value="blocked">blocked</option>
    </Input>
  );
}

export default SearchBoxStatus;
