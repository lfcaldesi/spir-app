import React, { Component } from "react";
import axios from "axios";
import "./style.css";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  FormGroup,
  Label
} from "reactstrap";
import "rc-collapse/assets/index.css";
import SearchBoxName from "./SearchBoxName";
import SearchBoxStatus from "./SearchBoxStatus";
import SearchBoxType from "./SearchBoxType";
import Conditional from "./Conditional";
import { Collapse } from "antd";
import AuthenticationService from "./AuthenticationService";

import Logout from "./logout";
const { Panel } = Collapse;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,

      searchName: "",
      searchStatus: "",
      searchType: "",
      spirs: [],

      newSpirModal: false,
      newSpirData: {
        name: "",
        spirStatus: "pending",
        spirType: "CSA"
      },

      editSpirModal: false,
      editSpirData: {
        springProjectId: "",
        name: "",
        spirStatus: "",
        spirType: ""
      }
    };
    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];
    this.logout = this.logout.bind(this);
    this.resetTimeout = this.resetTimeout.bind(this);

    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }

    this.setTimeout();
  }

  //timeout logout
  clearTimeout() {
    if (this.warnTimeout) clearTimeout(this.warnTimeout);

    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  }

  setTimeout() {
    this.logoutTimeout = setTimeout(this.logout, 30 * 1000);
  }

  resetTimeout() {
    this.clearTimeout();
    this.setTimeout();
  }

  logout() {
    alert("Session expired.Logging out...");
    console.log("Session expired");
    AuthenticationService.logout();
    this.props.history.push(`/login`);
    console.log("logout successfully");
  }

  destroy() {
    this.clearTimeout();

    for (var i in this.events) {
      window.removeEventListener(this.events[i], this.resetTimeout);
    }
  }

  handleChangeName = event => {
    this.setState({ searchName: event.target.value });
  };

  handleChangeStatus = event => {
    this.setState({ searchStatus: event.target.value });
  };

  handleChangeType = event => {
    this.setState({ searchType: event.target.value });
  };
  resetFilters = e => {
    this.setState({
      searchName: (e.target.value = ""),
      searchStatus: (e.target.value = ""),
      searchType: (e.target.value = "")
    });
  };

  //NATIVE FILTER
  handleChangeFindByStatus = e => {
    console.log(e.target.value);

    if (e.target.value === "") {
      this._refreshSpirs();
    } else {
      this.getSpirByStatus(e.target.value);
    }
  };

  //READ
  componentDidMount() {
    setTimeout(() => {
      this.setState({ isLoading: false }, this._refreshSpirs());
    }, 1500);
  }
  _refreshSpirs() {
    axios
      .get("http://localhost:8080/spir")
      .then(response => this.setState({ spirs: response.data }));
  }

  //NATIVE STATUS SEARCH
  getSpirByStatus(spirStatus) {
    axios
      .get("http://localhost:8080/spir/native/" + spirStatus)
      .then(response => this.setState({ spirs: response.data }));
  }

  //CREATE
  toggleNewSpirModal() {
    this.setState({ newSpirModal: !this.state.newSpirModal });
  }

  addSpir() {
    axios
      .post("http://localhost:8080/spir", this.state.newSpirData)
      .then(response => {
        //debug-->console.log("newSpirData name is :" + this.state.newSpirData.name);
        //if (this.state.newSpirData.name === "") alert("name cannot be null");
        if (this.state.newSpirData.name.length < 4)
          alert("name minimun of 4 character");
        else {
          let { spirs } = this.state;
          spirs.push(response.data);

          this.setState({
            spirs,
            newSpirModal: false,
            newSpirData: {
              name: "",
              spirStatus: "",
              spirType: ""
            }
          });
        }
      });
  }

  //UPDATE
  toggleEditSpirModal() {
    this.setState({ editSpirModal: !this.state.editSpirModal });
  }

  updateSpir() {
    let { name, spirType, spirStatus } = this.state.editSpirData;
    axios
      .put(
        "http://localhost:8080/spir/" + this.state.editSpirData.springProjectId,
        { name, spirType, spirStatus }
      )
      .then(response => {
        this._refreshSpirs();
        this.setState({
          editSpirModal: false,
          editSpirData: {
            springProjectId: "",
            name: "",
            spirType: "",

            spirStatus: ""
          }
        });
      });
  }
  generateExcel(springProjectId) {
    window.open(
      "http://localhost:8080/spir/download/" + springProjectId,
      "_blank"
    );
  }

  generateTotalExcel() {
    window.open("http://localhost:8080/spir/download/", "_blank");
  }

  editSpir(springProjectId, name, spirType, spirStatus) {
    this.setState({
      editSpirData: { springProjectId, name, spirType, spirStatus },
      editSpirModal: !this.state.editSpirModal
    });
  }

  deleteSpir(springProjectId) {
    var retVal = window.confirm("Are you sure you want to delete the Spir ?");
    if (retVal === true) {
      axios
        .delete("http://localhost:8080/spir/" + springProjectId)
        .then(response => {
          alert("Spir deleted successfully");
          this._refreshSpirs();
        });
    } else {
      alert("Operation canceled!");
    }
  }

  cloneSpir(springProjectId) {
    var retVal = window.confirm("Are you sure you want to clone the Spir ?");
    if (retVal === true) {
      axios
        .post("http://localhost:8080/spir/" + springProjectId)
        .then(response => {
          alert("Spir cloned successfully");
          this._refreshSpirs();
        });
    } else {
      alert("Operation canceled!");
    }
  }

  render() {
    let spirsList = this.state.spirs;
    if (this.state.searchName) {
      spirsList = spirsList.filter(spir => {
        return spir.name
          .toLowerCase()
          .startsWith(this.state.searchName.toLowerCase());
      });
    } //end 1st if

    if (this.state.searchStatus) {
      spirsList = spirsList.filter(spir =>
        spir.spirStatus
          .toLowerCase()
          .startsWith(this.state.searchStatus.toLowerCase())
      );
    }

    if (this.state.searchType) {
      spirsList = spirsList.filter(spir =>
        spir.spirType
          .toLowerCase()
          .startsWith(this.state.searchType.toLowerCase())
      );
    }

    //timer logout

    return (
      <div className="App container">
        <Conditional isLoading={this.state.isLoading} />
        <Logout></Logout>
        <Button
          className="my-3"
          color="primary"
          onClick={this.toggleNewSpirModal.bind(this)}
        >
          Add Spir
        </Button>

        <Button
          className="my-3"
          color="primary"
          onClick={this.generateTotalExcel.bind(this)}
        >
          Excel Report
        </Button>

        <Collapse>
          <Panel header="Filters" key="1">
            <p>
              {" "}
              <SearchBoxName handleChangeName={this.handleChangeName} />
              <SearchBoxType handleChangeType={this.handleChangeType} />
              <SearchBoxStatus handleChangeStatus={this.handleChangeStatus} />
              <Button
                className="my-3"
                color="primary"
                onClick={this.resetFilters}
              >
                Reset Filters
              </Button>
            </p>
          </Panel>
        </Collapse>

        <Modal
          isOpen={this.state.newSpirModal}
          toggle={this.toggleNewSpirModal.bind(this)}
        >
          <ModalHeader toggle={this.toggleNewSpirModal.bind(this)}>
            Add new Spir
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="name">Spir Name</Label>
              <Input
                id="name"
                required
                value={this.state.newSpirData.name}
                onChange={e => {
                  let { newSpirData } = this.state;
                  newSpirData.name = e.target.value;
                  this.setState({ newSpirData });
                }}
              ></Input>

              <Label for="type">Spir Type</Label>
              <Input
                type="select"
                id="type"
                value={this.state.newSpirData.spirType}
                onChange={e => {
                  let { newSpirData } = this.state;
                  newSpirData.spirType = e.target.value;
                  this.setState({ newSpirData });
                }}
              >
                <option value="CSA">CSA</option>
                <option value="Transactional">Transactional</option>
              </Input>

              <Label for="status">Spir Status</Label>
              <Input
                type="select"
                id="status"
                value={this.state.newSpirData.spirStatus}
                onChange={e => {
                  let { newSpirData } = this.state;
                  newSpirData.spirStatus = e.target.value;
                  this.setState({ newSpirData });
                }}
              >
                <option value="pending">pending</option>
                <option value="blocked">blocked</option>
                <option value="active">active</option>
              </Input>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button
              disabled={this.state.newSpirData.name === ""}
              color="primary"
              onClick={this.addSpir.bind(this)}
            >
              Create Spir
            </Button>

            <Button
              color="secondary"
              onClick={this.toggleNewSpirModal.bind(this)}
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>

        <Collapse accordion={false}>
          <Panel header="Backend Filter by status" key="1">
            <p>
              <Input
                type="select"
                id="native"
                onChange={this.handleChangeFindByStatus}
              >
                <option value="">--All Spirs--</option>
                <option value="pending">pending</option>
                <option value="blocked">blocked</option>
              </Input>
            </p>
          </Panel>
        </Collapse>
        <Modal
          isOpen={this.state.editSpirModal}
          toggle={this.toggleEditSpirModal.bind(this)}
        >
          <ModalHeader toggle={this.toggleEditSpirModal.bind(this)}>
            Edit Spir
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="name">Spir Name</Label>
              <Input
                id="name"
                value={this.state.editSpirData.name}
                onChange={e => {
                  let { editSpirData } = this.state;
                  editSpirData.name = e.target.value;
                  this.setState({ editSpirData });
                }}
              ></Input>

              <Label for="type">Spir Type</Label>
              <Input
                type="select"
                id="type"
                value={this.state.editSpirData.spirType}
                onChange={e => {
                  let { editSpirData } = this.state;
                  editSpirData.spirType = e.target.value;
                  this.setState({ editSpirData });
                }}
              >
                <option value="CSA">CSA</option>
                <option value="Transactional">Transactional</option>
              </Input>

              <Label for="status">Spir Status</Label>
              <Input
                type="select"
                id="status"
                value={this.state.editSpirData.spirStatus}
                onChange={e => {
                  let { editSpirData } = this.state;
                  editSpirData.spirStatus = e.target.value;
                  this.setState({ editSpirData });
                }}
              >
                <option value="active">active</option>
                <option value="pending">pending</option>
                <option value="blocked">blocked</option>
              </Input>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.updateSpir.bind(this)}>
              Update Spir
            </Button>

            <Button
              color="secondary"
              onClick={this.toggleEditSpirModal.bind(this)}
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <div className="row">
          {spirsList.map(spir => (
            <div className="col-sm-4" key={spir.springProjectId}>
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Spir{spir.springProjectId}</h5>
                  <p className="card-text">Name: {spir.name}</p>

                  <div class="inline">
                    {spir.spirStatus === "pending" && (
                      <p className="oval-orange-status"></p>
                    )}
                    {spir.spirStatus === "active" && (
                      <p className="oval-green-status"></p>
                    )}
                    {spir.spirStatus === "blocked" && (
                      <p className="oval-red-status"></p>
                    )}

                    <p className="card-text">Status: {spir.spirStatus}</p>
                  </div>

                  <p className="card-text">Type: {spir.spirType}</p>
                  <Collapse>
                    <Panel header="Actions" key="1">
                      <Button
                        color="info"
                        size="sm"
                        className="mr-2"
                        onClick={this.editSpir.bind(
                          this,
                          spir.springProjectId,
                          spir.name,
                          spir.spirType,
                          spir.spirStatus
                        )}
                      >
                        Edit
                      </Button>
                      <Button
                        color="info"
                        size="sm"
                        className="mr-2"
                        onClick={this.generateExcel.bind(
                          this,
                          spir.springProjectId
                        )}
                      >
                        Excel
                      </Button>
                      <Button
                        color="danger"
                        size="sm"
                        onClick={this.deleteSpir.bind(
                          this,
                          spir.springProjectId
                        )}
                      >
                        Delete
                      </Button>
                      {spir.spirStatus === "blocked" ? null : (
                        <Button
                          color="info"
                          size="sm"
                          onClick={this.cloneSpir.bind(
                            this,
                            spir.springProjectId
                          )}
                        >
                          Clone
                        </Button>
                      )}
                    </Panel>
                  </Collapse>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default App;
