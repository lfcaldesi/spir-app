import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import * as serviceWorker from "./serviceWorker";
import {
  Route,
  withRouter,
  Switch,
  BrowserRouter as Router
} from "react-router-dom";
import NotFoundComponent from "./NotFoundComponent";
import Login from "./login";
import AuthenticatedRoute from "./AuthenticatedRoute";

const routing = (
  <Router>
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/login" exact component={Login} />

      <AuthenticatedRoute path="/spirs" exact component={App} />

      <AuthenticatedRoute path="*" component={NotFoundComponent} />
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));

serviceWorker.unregister();

export default withRouter(App);
