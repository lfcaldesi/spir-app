import React from "react";
import { Table, Button } from "reactstrap";

const Spirs = ({ spirsList }) => {
  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Type</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {spirsList.map(spir => (
          <tr key={spir.springProjectId}>
            <td>{spir.springProjectId}</td>
            <td>{spir.name}</td>
            <td>{spir.spirType}</td>
            <td>{spir.spirStatus}</td>

            <td>
              <Button
                color="info"
                size="sm"
                className="mr-2"
                onClick={this.editSpir.bind(
                  this,
                  spir.springProjectId,
                  spir.name,
                  spir.spirType,
                  spir.spirStatus
                )}
              >
                Edit
              </Button>
              <Button
                color="danger"
                size="sm"
                onClick={this.deleteSpir.bind(this, spir.springProjectId)}
              >
                Delete
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default Spirs;
