import React from "react";
import { Link } from "react-router-dom";
function NotFoundComponent() {
  return (
    <div>
      <h1>ERROR: PAGE NOT FOUND</h1>
      <p>
        <Link to="/spirs">GO TO -- Spirs List --</Link>
      </p>
    </div>
  );
}
export default NotFoundComponent;
