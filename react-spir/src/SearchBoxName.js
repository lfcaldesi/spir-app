import React from "react";
import { Input } from "reactstrap";
function SearchBoxName(props) {
  return (
    <Input
      type="text"
      onChange={props.handleChangeName}
      placeholder="Search Spir by name"
    />
  );
}

export default SearchBoxName;
