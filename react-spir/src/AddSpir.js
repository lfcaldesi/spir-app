import React from "react";
import {
  Button,
  ModalBody,
  ModalFooter,
  Input,
  FormGroup,
  Label,
  Modal
} from "reactstrap";

const AddSpir = props => {
  return (
    <Modal>
      <ModalBody>
        <FormGroup>
          <Label for="name">Spir Name</Label>
          <Input
            id="name"
            required
            value={this.state.newSpirData.name}
            onChange={e => {
              let { newSpirData } = this.state;
              newSpirData.name = e.target.value;
              this.setState({ newSpirData });
            }}
          ></Input>

          <Label for="type">Spir Type</Label>
          <Input
            type="select"
            id="type"
            value={this.state.newSpirData.spirType}
            onChange={e => {
              let { newSpirData } = this.state;
              newSpirData.spirType = e.target.value;
              this.setState({ newSpirData });
            }}
          >
            <option value="CSA">CSA</option>
            <option value="Transactional">Transactional</option>
          </Input>

          <Label for="status">Spir Status</Label>
          <Input
            type="select"
            id="status"
            value={this.state.newSpirData.spirStatus}
            onChange={e => {
              let { newSpirData } = this.state;
              newSpirData.spirStatus = e.target.value;
              this.setState({ newSpirData });
            }}
          >
            <option value="pending">pending</option>
            <option value="blocked">blocked</option>
            <option value="active">active</option>
          </Input>
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button
          disabled={this.state.newSpirData.name === ""}
          color="primary"
          onClick={this.addSpir.bind(this)}
        >
          Create Spir
        </Button>

        <Button color="secondary" onClick={this.toggleNewSpirModal.bind(this)}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddSpir;
