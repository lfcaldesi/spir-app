import React from "react";
import { Input } from "reactstrap";
function SearchBoxType(props) {
  return (
    <Input type="select" onChange={props.handleChangeType}>
      <option value="">--Search by type--</option>
      <option value="CSA">CSA</option>
      <option value="Transactional">Transactional</option>
    </Input>
  );
}

export default SearchBoxType;
